
{% load static %}

<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <title>Florii</title>
</head>

<body>
  <ul>
    {% for floare in florii %}
    <li>
      {{floare.title}}
      <ul>
        <li><a href="/{{floare.code}}">/{{floare.code}}</a></li>
      </ul>
    </li>
    {% endfor %}
    <li>
      エラーになる、存在しないページ 
      <ul>
        <li><a href="/notfound">/notfound</a></li>
      </ul>
    </li>
  </ul>
  <img src="{% static 'hirumeshi.jpg' %}">
</body>

</html>
