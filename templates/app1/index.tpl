
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Index</title>
</head>

<body>
  <ul>
    <li><a href="/static_image/">ふつうにstatic画像を表示するページ</a></li>
    <li><a href="/static_image_dynamic/">動的にstatic画像を表示するページ</a></li>
    <li><a href="/htmlnized/">DBのMDをHTML化して表示するページ</a></li>
    <li><a href="/htmlnized_github/">↑のをgithub CSSで表示するページ</a></li>
    <li><a href="/karl_test/">karl表示するページ</a></li>
  </ul>
</body>

</html>
