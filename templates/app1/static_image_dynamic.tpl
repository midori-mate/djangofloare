
{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>static_image_dynamic</title>
</head>

<body>
  <p>
    static タグで floare.png を記述。<br>
    {% static png_path %}<br>
    <img src="{% static png_path %}">
  </p>
  <p>
    べた書きで floare.png を記述。<br>
    /static/{{png_path}}<br>
    <img src="/static/{{png_path}}">
  </p>
</body>

</html>
