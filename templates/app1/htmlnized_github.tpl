
{% load static %}

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>htmlnized</title>
  <link rel="stylesheet" href="{% static 'app1/css/github-markdown.css' %}">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/default.min.css">
</head>

<body class="markdown-body">
  {{row.text_to_markdown|safe}}
  <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"></script>
  <script>hljs.initHighlightingOnLoad();</script>
</body>

</html>
