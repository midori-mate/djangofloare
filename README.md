DjangoFloare
===

## Installation

```
$ git clone https://gitlab.com/midori-mate/djangofloare.git
```


## Open on virtual environment

Install them.

- [Vagrant](https://www.vagrantup.com/)
- [Virtualbox](https://www.virtualbox.org/)

**Windows user has to open Powershell as Administrator!** Or you cannot create symbolic links and setup will fail.

```
$ vagrant up    # Open virtual env. It would take 10 minutes long.
$ vagrant ssh   # Enter the virtual env.
```

In the virtual env.

```
$ source /env3.6/bin/activate
$ python /vagrant/manage.py createsuperuser             # Use this account only on your env.
```

Access [localhost:1992/](http://localhost:1992/) then.  
Access [localhost:1992/admin/](http://localhost:1992/admin/) as well.

Close the env.

```
$ exit                                                  # Exit from virtual env.
$ vagrant halt                                          # Close the virtual env.
```
